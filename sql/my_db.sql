/*
 Navicat Premium Data Transfer

 Source Server         : ZY.Node.MongoDB
 Source Server Type    : MongoDB
 Source Server Version : 40406
 Source Host           : 114.117.164.181:27017
 Source Schema         : my_db

 Target Server Type    : MongoDB
 Target Server Version : 40406
 File Encoding         : 65001

 Date: 11/04/2023 16:23:44
*/


// ----------------------------
// Collection structure for users
// ----------------------------
db.getCollection("users").drop();
db.createCollection("users");

// ----------------------------
// Documents of users
// ----------------------------
db.getCollection("users").insert([ {
    _id: ObjectId("643517dddb3a83a7f8a27a54"),
    username: "芒果快熟",
    password: "$2b$10$CRJnSNFjpAfcfLTo0/pTt.V9iGLWdlQ3t73Fe7CuMpU0K9dU9fHP.",
    email: "1840354092@qq.com",
    status: true,
    isConfirmed: true,
    otpTries: NumberInt("0"),
    createdAt: ISODate("2023-04-11T08:18:37.097Z"),
    updatedAt: ISODate("2023-04-11T08:19:11.793Z")
} ]);
